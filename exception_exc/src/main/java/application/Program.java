package application;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		
		method();

		System.out.println("End of the program");

	}
	
	public static void method() {
		
		Scanner sc = new Scanner(System.in);
		
		try {
			String[] vect = sc.nextLine().split(" ");
			int x = sc.nextInt();
			System.out.println(vect[x]);
		}
		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Position not found");
		}
		catch (InputMismatchException e) {
			System.out.println("Input error");
		}
		
	}
}
