package br.com.lillianlutzner;

import entities.Account;
import model.exception.LimitException;

public class Program {

	public static void main(String[] args) {
		
		Account acc = new Account(1008, "Lillian", 300.0, 100.0);
		//acc.deposit(50.0);
		System.out.println(acc.getBalance());
		
		try {
			acc.withdraw(100.0);
		} catch (LimitException e) {
			System.out.println(e.getMessage());;
		}
		
		System.out.println(acc.getBalance());
		

	}

}
